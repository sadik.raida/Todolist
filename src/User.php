<?php

namespace App;

class User
{
    private string $email;
    private string $nom;
    private string $prenom;
    private \DateTime $date_de_naissance;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return User
     */
    public function setNom(string $nom): User
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     * @return User
     */
    public function setPrenom(string $prenom): User
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateDeNaissance(): \DateTime
    {
        return $this->date_de_naissance;
    }

    /**
     * @param \DateTime $date_de_naissance
     * @return User
     */
    public function setDateDeNaissance(\DateTime $date_de_naissance): User
    {
        $this->date_de_naissance = $date_de_naissance;
        return $this;
    }

    public function isValid() {
        if($this->checkEmail() == false) { throw new \Exception('Email is empty'); }
        if($this->checkEmailFormat() == false) { throw new \Exception('Email format is invalid'); }
        if($this->checkNom() == false) { throw new \Exception('Nom is empty'); }
        if($this->checkPrenom() == false) { throw new \Exception('Prenom is empty'); }
        if($this->checkAge() == false) { throw new \Exception('Age under 13 years'); }

        return true;
    }


    private function checkEmail(): bool {
        return !empty($this->getEmail());
    }

    private function checkEmailFormat(): bool {
        return filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL) ? true : false;
    }

    private function checkNom(): bool {
        return !empty($this->getNom());
    }

    private function checkPrenom(): bool {
        return !empty($this->getPrenom());
    }

    private function checkAge(): int {
        $age = $this->getDateDeNaissance()->diff(new \DateTime('now'));

        /* format in year https://www.php.net/manual/en/datetime.createfromformat.php */
        return $age->format('y') >= 13 ? true : false;
    }

}
