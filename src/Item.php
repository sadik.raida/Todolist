<?php

namespace App;

class Item
{
    
    private string $name;
    private string $content;
    private \DateTime $date_creation;

    function __construct( string $name, string $content) {
        $date_creation = new \DateTime('now');
        if ($this->checkContent($content)){
            $content=$content;
        }
        $this->name = $this->name; 
    }

    public function checkContent(string $content){
        if(strlen($content)>1000){
            return false;
        }
        else{
            return true;
        }
    }

    public function getName(){
        return $this->name;
    }

    public function getContent(){
        return $this->content;
    }
    
    public function getDateCreation(){
        return $this->date_creation;
    }

}