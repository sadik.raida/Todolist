<?php

namespace App\tests;

use App\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $user = new User();

        $dt = new \DateTime();
        $dt->setDate('2017', '05', '23');

        $this->user = $user
            ->setEmail('monemail@gmail.com')
            ->setNom('JACKSON')
            ->setPrenom('Michael')
            ->setDateDeNaissance($dt);
    }

    public function test_checkIsValidTrue() {
        $this->assertTrue($this->user->isValid());
    }

    public function test_checkEmptyEmail() {
        $this->user->setEmail('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Email is empty');

        $this->user->isValid();
    }

    public function test_checkEmptyNom() {
        $this->user->setNom('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Nom is empty');

        $this->user->isValid();
    }

    public function test_checkEmptyPrenom() {
        $this->user->setPrenom('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Prenom is empty');

        $this->user->isValid();
    }

    public function test_checkAgeUnder13yearsOld() {

        $dt = new \DateTime();
        $dt->setDate('2020', '05', '23');

        $this->user->setDateDeNaissance($dt);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Age under 13 years');

        $this->user->isValid();
    }
}
