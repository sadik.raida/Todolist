<?php

namespace App\tests;

use App\Todo;
use App\Item;
use PHPUnit\Framework\TestCase;

class TodoTest extends TestCase
{
    private Todo $todolist;

    public function __construct()
    {
        
        $todo = new Todo();

        $item1 = new Item("Courses", "fromage, lait, fraise");
        $item2 = new Item("Sport", "Courir 38 minutes");
        $item3 = new Item("Soirée", "dormir tot");

        $todo->addItem($item1);
        $todo->addItem($item3);
        $todo->addItem($item2);
    }

    public function test_checkIsValidTrue() {
        $this->assertTrue($this->user->isValid());
    }

    public function test_checkEmptyEmail() {
        $this->user->setEmail('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Email is empty');

        $this->user->isValid();
    }

    public function test_checkEmptyNom() {
        $this->user->setNom('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Nom is empty');

        $this->user->isValid();
    }

    public function test_checkEmptyPrenom() {
        $this->user->setPrenom('');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Prenom is empty');

        $this->user->isValid();
    }

    public function test_checkAgeUnder13yearsOld() {

        $dt = new \DateTime();
        $dt->setDate('2020', '05', '23');

        $this->user->setDateDeNaissance($dt);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Age under 13 years');

        $this->user->isValid();
    }
}